import {
  OrientDBClient,
  ODatabaseSessionPool,
  OrientDBClientConfig,
  ODatabaseSessionOptions
} from 'orientjs';
import { from, of, throwError, timer } from 'rxjs';
import { delayWhen, map, retryWhen, switchMap, tap } from 'rxjs/operators';
import { OModel } from './lib/OModel';
import { OSchema } from './lib/OSchema';
import { SchemaInitial } from './types/SchemaInitial';
import { genericRetryStrategy } from './helpers/generateRetryStrategy';
import { createOResultWrapper } from './lib/OResultWrapper';

const _models = new Map();
let _wrappers: Map<string, any> = new Map();

let _client: OrientDBClient | null = null;
let _pool: ODatabaseSessionPool | null = null;

export class ONeo {
  private _client: OrientDBClient | null = null;
  private _pool: ODatabaseSessionPool | null = null;
  private _models = new Map();

  static model(name: string, schema: SchemaInitial) {
    if (!(schema instanceof OSchema)) {
      throw new Error('Schema MUST be an Instance of "OrientDbSchema"');
    }
    if (_models.has(name)) {
      throw new Error(`Model "${name}" already added`);
    }
    const model = new OModel(ONeo.session$, name, schema, () => _wrappers);
    _wrappers.set(name, createOResultWrapper(schema));
    _models.set(name, model);
    return model;
  }

  static models(name: string) {
    return name ? _models.get(name) : _models;
  }

  model(name: string, schema: SchemaInitial) {
    if (!(schema instanceof OSchema)) {
      throw new Error('Schema MUST be an Instance of "OrientDbSchema"');
    }
    if (this._models.has(name)) {
      throw new Error(`Model "${name}" already added`);
    }

    const model = new OModel(this.session$, name, schema, () => _wrappers);
    this._models.set(name, model);
    _wrappers.set(name, createOResultWrapper(schema));
    return model;
  }

  models(name: string) {
    return name ? this._models.get(name) : this._models;
  }

  static get pool$() {
    return of(_pool).pipe(
      switchMap(() => {
        if (_pool) {
          return of(_pool);
        }
        return throwError(new Error());
      }),
      retryWhen((errors) => errors.pipe(delayWhen(() => timer(500))))
    );
  }

  static get session$() {
    return ONeo.pool$.pipe(
      switchMap((pool) => from(pool.acquire())),
      retryWhen((errors) => errors.pipe(delayWhen(() => timer(500)))),
      switchMap((session) => from(session.close()).pipe(map(() => session)))
    );
  }

  get pool$() {
    return of(this._pool).pipe(
      switchMap(() => {
        if (this._pool) {
          return of(this._pool);
        }
        return throwError(new Error());
      }),
      retryWhen((errors) => errors.pipe(delayWhen(() => timer(500))))
    );
  }

  get session$() {
    return this.pool$.pipe(
      switchMap((pool) => from(pool.acquire())),
      retryWhen((errors) => errors.pipe(delayWhen(() => timer(500)))),
      switchMap((session) => from(session.close()).pipe(map(() => session)))
    );
  }

  static connect(
    client: OrientDBClientConfig,
    session: ODatabaseSessionOptions
  ) {
    return new ONeo()._connect(client, session);
  }

  _connect(client: OrientDBClientConfig, session: ODatabaseSessionOptions) {
    return from(OrientDBClient.connect(client)).pipe(
      retryWhen(
        genericRetryStrategy({ maxRetryAttempts: 3, scalingDuration: 500 })
      ),
      switchMap((client) => {
        _client = _client || client;

        Object.defineProperty(this, '_client', {
          enumerable: false,
          value: client
        });

        return from(client.sessions(session)).pipe(
          retryWhen(genericRetryStrategy()),
          tap((pool) => {
            _pool = _pool || pool;
            Object.defineProperty(this, '_pool', {
              enumerable: false,
              value: pool
            });
          })
        );
      }),
      map(() => this)
    );
  }
}
