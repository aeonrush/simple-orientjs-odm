import { defineHiddenReadonlyProperty } from '../helpers/defineProperty';
import { SchemaInitial } from '../types/SchemaInitial';

export class OSchema {
  public virtuals: { [key: string]: Function } = {};
  private _schema = {};
  /**
   * @param schema
   */
  constructor(schema: SchemaInitial) {
    defineHiddenReadonlyProperty(this, '_schema', schema);
  }
  get schema() {
    return this._schema;
  }
}
