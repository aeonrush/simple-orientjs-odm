import { ODatabaseSession, ODatabaseSessionPool } from 'orientjs';
import { Observable, of } from 'rxjs';
import { defineHiddenReadonlyProperty } from '../helpers/defineProperty';
import { OQueryBuilder } from './OQueryBuilder';

export class OQuery {
  static findOne(
    session$: Observable<ODatabaseSession>,
    wrapper: any,
    id: any,
    projection: string | Array<string>
  ) {
    return new OQuery(session$, wrapper, {
      id,
      projection,
      params: {},
      kind: 'findById'
    });
  }

  static find(
    session$: Observable<ODatabaseSession>,
    wrapper: any,
    name: string,
    query: any,
    projection: string | Array<string>
  ) {
    return new OQuery(session$, wrapper, {
      name,
      query,
      projection,
      params: {},
      kind: 'find'
    });
  }

  private _query: { [key: string]: any } = {};
  private _wrapper: any;

  constructor(
    private session$: Observable<ODatabaseSession>,
    wrapper: any,
    query: any
  ) {
    defineHiddenReadonlyProperty(this, '_query', query);
    defineHiddenReadonlyProperty(this, '_wrapper', wrapper);
  }

  limit(limit: number) {
    this._query.params.limit = limit;
    return this;
  }

  skip(skip: number) {
    this._query.params.skip = skip;
    return this;
  }

  async command() {
    const query = OQueryBuilder.build(this._query);
    if (!query) {
      throw new Error(`Build query failed`);
    }

    const session = await this.session$.toPromise();

    const command = session.command(query.query);
    const _all = command.all;

    command.all = () => {
      return _all.call(command).then((data) =>
        data.map((d: any) => {
          if (!d) {
            return null;
          }
          const wrapper = this._wrapper().get(d['@class']);
          if (!wrapper) {
            return d;
          }
          return new wrapper(d);
        })
      );
    };

    return command;
  }

  async one() {
    const command = await this.command();
    return command.one();
  }

  async all() {
    const command = await this.command();
    return command.all();
  }
}
