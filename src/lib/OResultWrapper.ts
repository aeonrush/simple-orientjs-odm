import { defineHiddenProperty } from '../helpers/defineProperty';
import { SchemaInitial } from '../types/SchemaInitial';

const inspect = Symbol.for('nodejs.util.inspect.custom');

const hasConflict = (obj: Object, key: string) =>
  Object.prototype.hasOwnProperty.call(obj, key);

export const createOResultWrapper = (schema: SchemaInitial) =>
  class OResultWrapper<T> {
    _changes: { [key: string]: any } = {};
    _rawResult: { [key: string]: any } = {};

    constructor(result: T) {
      defineHiddenProperty(this, '_changes', { ...result });
      defineHiddenProperty(this, '_rawResult', { ...result });

      Object.keys(result).forEach((key) => {
        Object.defineProperty(this, key, {
          enumerable: true,
          get: () => {
            return this._changes[key] || this._rawResult[key];
          },
          set: (value) => {
            this._changes[key] = value;
          }
        });
      });

      Object.keys(schema.virtuals || {}).forEach((key) => {
        if (hasConflict(this, key)) {
          throw new Error(`Schema already has this field "${key}"`);
        }
        Object.defineProperty(this, key, {
          enumerable: true,
          get: () =>
            schema.virtuals[key].bind(this)({
              ...this._rawResult,
              ...this._changes
            })
        });
      });
    }

    [inspect]() {
      return { ...this, ...this._changes };
    }

    toObject() {
      return this[inspect]();
    }

    toJSON() {
      return this[inspect]();
    }

    get id() {
      return this._rawResult['@rid'];
    }
  };
