import { ODatabaseSession } from 'orientjs';
import { Observable } from 'rxjs';
import { defineHiddenReadonlyProperty } from '../helpers/defineProperty';
import { SchemaInitial } from '../types/SchemaInitial';
import { OQuery } from './OQuery';
import { createOResultWrapper } from './OResultWrapper';

export class OModel {
  private _name: string = '';
  private _wrapper: any = null;
  private _session$: any;
  private _wrappers: () => Map<string, any> = () => new Map();

  constructor(
    session$: Observable<ODatabaseSession>,
    name: string,
    schema: SchemaInitial,
    wrappers: () => Map<string, any>
  ) {
    defineHiddenReadonlyProperty(this, '_name', name);
    // defineHiddenReadonlyProperty(
    //   this,
    //   '_wrapper',
    //   createOResultWrapper(schema)
    // );
    defineHiddenReadonlyProperty(this, '_session$', session$);
    defineHiddenReadonlyProperty(this, '_wrappers', wrappers);
  }

  async findById(id: string, projection: string | Array<string> = '*') {
    if ('string' !== typeof projection && !Array.isArray(projection)) {
      throw new Error('Projection should be an Array or a String');
    }

    const query = await OQuery.findOne(
      this._session$,
      this._wrappers,
      id,
      Array.isArray(projection) ? projection : [projection]
    );

    const command = await query.command();
    return await command.one();
  }

  find(query: any, projection: string | Array<string> = '*') {
    return OQuery.find(
      this._session$,
      this._wrappers,
      this._name,
      query,
      Array.isArray(projection) ? projection : [projection]
    );
  }
}
