export class OQueryBuilder {
  static build(query: any) {
    // TODO: Add query interface
    switch (query.kind) {
      case 'findById': {
        return {
          operator: 'one',
          query: `select from #${query.id}`
        };
      }

      case 'find': {
        const p = query.projection.filter((_: string) => !!_).join(',');
        let limit = '';

        if (query.params.limit) {
          limit = `LIMIT ${query.params.limit}`;
        }

        let skip = '';
        if (query.params.skip) {
          skip = `SKIP ${query.params.skip}`;
        }

        return {
          query: `select ${p} from ${query.name} ${limit} ${skip}`
        };
      }

      default: {
        return null;
      }
    }
  }
}
