import { ONeo } from '..';
import { OSchema } from '..';

const User = new OSchema({
  username: true
});

User.virtuals.test = function() {
  console.log('-'.repeat(30));
  return this['username'];
};

export const user = ONeo.model('User', User);
