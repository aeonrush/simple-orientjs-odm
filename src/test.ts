import { ONeo } from '.';
import { user } from './test/user.vertex';

user
  .find({ test: 123 })
  .skip(0)
  .limit(10)
  .all()
  .then((data) => {
    console.log('First request', data);
  })
  .catch((e) => console.error(e));

const clientConfig = {
  host: 'localhost',
  port: 2424,
  pool: {
    max: 100
  }
};
const sessionOptions = {
  name: 'easyblog',
  username: 'root',
  password: 'root'
};

ONeo.connect(clientConfig, sessionOptions).subscribe(undefined, (e) =>
  console.error(e)
);
