export const defineHiddenReadonlyProperty = (
  obj: Object,
  key: string,
  value: any
): void => {
  Object.defineProperty(obj, key, {
    value,
    enumerable: false,
    writable: false
  });
};

export const defineHiddenProperty = (
  obj: Object,
  key: string,
  value: any
): void => {
  Object.defineProperty(obj, key, {
    value,
    enumerable: false
  });
};
